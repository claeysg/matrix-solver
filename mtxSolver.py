#!/usr/bin/env python3
import numpy as np
import sys 
import math
import re
import contextlib

@contextlib.contextmanager
def printoptions(*args, **kwargs):
    original = np.get_printoptions()
    np.set_printoptions(*args, **kwargs)
    try:
        yield
    finally: 
        np.set_printoptions(**original)

"""
Project Goals:
1. Validate string input of equations
2. Parse ints and var names to build matrix
3. use numpy to solve with Gaussian elimination
4. Implement solving with Gauss- Seidel
5. Implement solving with the cooler one after that
6. Allow loading of .csv files
7. Time to solve tracking
8. GUI
9. Web?
"""

def handleInput():
    isValidIn = False
    #TODO: throw error if vars dont have coefficient or no solution var
    #TODO: use optional parameters to validate if variables are right, enforce that in order
    #TODO: use optional parameters to enforce consistent size

    while (isValidIn == False):
        isValidIn = True
        eq1 = input("input equation\n")
        tableau1 = []
        vars = []

        tableau1 = re.findall('\d+', eq1)
        rowLen= len(tableau1)

        b = tableau1[-1]
        tableau1 = tableau1[:-1]
        vars = re.findall('[a-zA-z]+', eq1)

        if "=" not in eq1:
            print("Must have an equals")
            isValidIn = False

        if (rowLen < 3):
            print("Matrix of insufficient size")
            isValidIn = False

        if((len(vars) != rowLen - 1)):
            print("must have same number of varaibles and numbers")
            isValidIn = False

    return [tableau1, vars, b]


def getInput():
    #TODO: splitup b here?
    rslt1 = handleInput()
    tableau1 = rslt1[0]
    vars = rslt1[1]
    b = rslt1[2]
    n = len(tableau1) 
    matrices = [None] * n
    sltns = [None] * n

    matrices[0] = (tableau1)
    sltns[0] = (b)



    print("now must enter " + str(n - 1) + " more equation(s) to")
    print("complete the " + str(n) + "x" + str(n) + " matrix")

    for i in range(1, n):
        rslt = handleInput()
        matrices[i] = (rslt[0])
        sltns[i] = rslt[2] 
    return np.asarray([matrices, vars, sltns])

def solutionRunner(A, vars, B):
    #TODO: decide best matrix algorithm
    solveGS(A, vars, B)

def solveGS(A, B, vars):
    print("Solving with Gauss- Seidel... \n\n")
    n = len(A) - 1
    

    for j in range(1, n -1):
        for i in range(i+1, n):
            mult = A[i][j]/A[j][j]
            for k in range(j+1, n): 
                A[i][k] = A[i][k] - mult * A[j][k]
            B[i] = B[i] - mult * B[j] 

    print(A)
    print(B)

if __name__ == "__main__":
    print("Enter systems to solve:")
    print("Must be in format like 3u + 5v + 7k = 2")
    print("For now must be a square matrix")
    #print("Someday soon will use must efficient method to solve")
    print("")

    rawInput = getInput()
    #with printoptions(precision=3, suppress=True):
        #print(rawInput[0])
        #print(rawInput)
    solutionRunner(rawInput[0], rawInput[1], rawInput[2])

